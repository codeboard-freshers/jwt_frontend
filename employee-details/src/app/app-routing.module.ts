import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { AuthGaurdService } from './service/auth-gaurd.service';

const routes: Routes = [
  { path: '', component: LoginComponent,canActivate:[AuthGaurdService]},
{path:'employees',component:EmployeeComponent,canActivate:[AuthGaurdService]},
{ path: 'login', component: LoginComponent},
{ path: 'logout', component: LogoutComponent ,canActivate:[AuthGaurdService]},
{ path:'register', component:RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
